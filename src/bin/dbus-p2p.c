#include <gio/gio.h>
#include <libslirp.h>
#include "dbus-vmstate1.h"

static char *id;
static char *dbus_addr;
static Slirp *slirp;
static GMutex mutex;
static GCond cond;

typedef struct ReadCb {
    const void *data;
    size_t left;
} ReadCb;

static ssize_t read_cb(void *buf, size_t len, void *opaque)
{
    ReadCb *cb = opaque;

    len = MIN(cb->left, len);
    memcpy(buf, cb->data, len);
    cb->left -= len;
    cb->data += len;

    return len;
}

static gboolean
vmstate_load(VMState1 *object G_GNUC_UNUSED, GDBusMethodInvocation *invocation,
             const gchar *arg_data G_GNUC_UNUSED, gpointer user_data G_GNUC_UNUSED)
{
    GVariant *args, *var;
    int version;
    ReadCb cb;

    args = g_dbus_method_invocation_get_parameters(invocation);
    var = g_variant_get_child_value(args, 0);
    cb.data = g_variant_get_fixed_array(var, &cb.left, sizeof(char));
    g_assert(cb.left >= sizeof(int));

    memcpy(&version, cb.data, sizeof(int));
    g_assert(GINT32_FROM_BE(version) <= slirp_state_version());
    cb.left -= sizeof(int);
    cb.data += sizeof(int);

    g_mutex_lock(&mutex);
    slirp_state_load(slirp, 0, read_cb, &cb);
    g_cond_signal(&cond);
    g_mutex_unlock(&mutex);
    g_variant_unref(var);

    g_dbus_method_invocation_return_value(invocation, g_variant_new("()"));
    return TRUE;
}

static ssize_t write_cb(const void *buf, size_t len, void *opaque G_GNUC_UNUSED)
{
    GByteArray *a = opaque;
    g_byte_array_append(a, buf, len);
    return len;
}

static gboolean
vmstate_save(VMState1 *object G_GNUC_UNUSED, GDBusMethodInvocation *invocation,
             gpointer user_data G_GNUC_UNUSED)
{
    GVariant *var;
    GByteArray *a = g_byte_array_new();
    int version = GINT32_TO_BE(slirp_state_version());

    g_byte_array_append(a, (void *)&version, sizeof(int));

    g_mutex_lock(&mutex);
    /* FIXME: lock rust-side.. */
    slirp_state_save(slirp, write_cb, a);
    g_mutex_unlock(&mutex);

    var = g_variant_new_fixed_array(G_VARIANT_TYPE_BYTE,
                                    a->data, a->len, sizeof(char));
    g_dbus_method_invocation_return_value(invocation,
                                          g_variant_new("(@ay)", var));
    g_byte_array_unref(a);

    return TRUE;
}

static void
connection_closed(GDBusConnection *connection,
                  gboolean remote_peer_vanished G_GNUC_UNUSED,
                  GError *Error G_GNUC_UNUSED,
                  gpointer user_data G_GNUC_UNUSED)
{
    /* g_clear_object(&om); */
    g_clear_object(&connection);
}

static GDBusObjectManagerServer *
get_omserver(GDBusConnection *conn, gpointer user_data G_GNUC_UNUSED)
{
    GDBusObjectManagerServer *om;
    GDBusObjectSkeleton *sk;
    VMState1 *v;

    om = g_dbus_object_manager_server_new("/org/qemu");
    sk = g_dbus_object_skeleton_new("/org/qemu/VMState1");

    v = vmstate1_skeleton_new();
    g_object_set(v, "id", id, NULL);
    g_signal_connect(v, "handle-load", G_CALLBACK(vmstate_load), NULL);
    g_signal_connect(v, "handle-save", G_CALLBACK(vmstate_save), NULL);

    g_dbus_object_skeleton_add_interface(sk, G_DBUS_INTERFACE_SKELETON(v));
    g_dbus_object_manager_server_export(om, sk);
    g_dbus_object_manager_server_set_connection(om, conn);

    g_clear_object(&v);
    g_clear_object(&sk);

    return om;
}

static gboolean
on_new_connection(GDBusServer *server G_GNUC_UNUSED,
                  GDBusConnection *connection,
                  gpointer user_data G_GNUC_UNUSED)
{
    g_object_ref(connection);
    g_signal_connect(connection, "closed",
                     G_CALLBACK(connection_closed), NULL);
    get_omserver(connection, NULL);

    return TRUE;
}

static gboolean
allow_mechanism_cb(GDBusAuthObserver *observer G_GNUC_UNUSED,
                   const gchar *mechanism,
                   gpointer user_data G_GNUC_UNUSED)
{
    return g_strcmp0(mechanism, "EXTERNAL") == 0;
}

static gboolean
authorize_authenticated_peer_cb(GDBusAuthObserver *observer G_GNUC_UNUSED,
                                GIOStream *stream G_GNUC_UNUSED,
                                GCredentials *credentials,
                                gpointer user_data G_GNUC_UNUSED)
{
    gboolean authorized = FALSE;

    if (credentials != NULL) {
        GCredentials *own_credentials = g_credentials_new();

        if (g_credentials_is_same_user(credentials, own_credentials, NULL)) {
            authorized = TRUE;
        }

        g_clear_object(&own_credentials);
    }

    return authorized;
}

static GDBusServer *
server_start(void)
{
    GDBusAuthObserver *observer = NULL;
    GDBusServer *server = NULL;
    gchar *guid = NULL;
    GError *error = NULL;

    guid = g_dbus_generate_guid();
    observer = g_dbus_auth_observer_new();
    g_signal_connect(observer, "allow-mechanism",
                     G_CALLBACK(allow_mechanism_cb), NULL);
    g_signal_connect(observer, "authorize-authenticated-peer",
                     G_CALLBACK(authorize_authenticated_peer_cb), NULL);

    server = g_dbus_server_new_sync(dbus_addr,
                                    G_DBUS_SERVER_FLAGS_NONE,
                                    guid,
                                    observer,
                                    NULL, /* GCancellable */
                                    &error);
    g_dbus_server_start(server);
    g_clear_object(&observer);
    g_free(guid);

    if (server == NULL) {
        g_printerr("Error creating server at address %s: %s\n",
                   dbus_addr, error->message);
        g_error_free(error);
        return NULL;
    }

    g_signal_connect(server, "new-connection",
                     G_CALLBACK(on_new_connection), NULL);

    return server;
}

static gpointer
dbus_thread(gpointer p G_GNUC_UNUSED)
{
    GMainContext *context = g_main_context_new();
    GMainLoop *loop = g_main_loop_new(context, FALSE);
    GDBusServer *server;

    g_main_context_push_thread_default(context);
    server = server_start();
    if (!server) {
        g_error("DBus p2p server failed to start");
    }

    g_main_loop_run(loop);
    g_main_loop_unref(loop);
    g_main_context_unref(context);
    g_clear_object(&server);

    return NULL;
}


void dbus_p2p(const char *addr, const char *theid, Slirp *s, int wait_load)
{
    id = g_strdup(theid);
    dbus_addr = g_strdup(addr);
    slirp = s;

    g_mutex_lock(&mutex);
    g_thread_new("dbus-p2p", dbus_thread, NULL);
    if (wait_load) {
        g_cond_wait(&cond, &mutex);
    }
    g_mutex_unlock(&mutex);
}
