use std::collections::HashMap;
use std::error::Error;
use std::ffi::CString;
use std::fs::File;
use std::io::{self, Read, Write};
use std::os::raw::{c_char, c_int, c_uint, c_void};
use std::os::unix::io::{FromRawFd, RawFd};
use std::os::unix::net::UnixDatagram;
use std::path::PathBuf;
use std::process;

use dbus;
use libc;
use libslirp;
use mio::unix::EventedFd;
use mio::unix::UnixReady;
use mio::*;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "slirp", about = "slirp helper process")]
struct Opt {
    /// Activate debug mode
    #[structopt(long)]
    debug: bool,
    /// Print capabilities
    #[structopt(long = "print-capabilities")]
    print_capabilities: bool,
    /// Exit with parent process
    #[structopt(long = "exit-with-parent")]
    exit_with_parent: bool,
    /// DBus bus address
    #[structopt(name = "address", long = "dbus-address")]
    dbus_address: Option<String>,
    /// DBus p2p server address
    #[structopt(name = "p2paddress", long = "dbus-p2p")]
    dbus_p2p: Option<String>,
    #[structopt(name = "id", long = "dbus-id")]
    dbus_id: Option<String>,
    /// Incoming migration data from DBus
    #[structopt(long = "dbus-incoming")]
    dbus_incoming: bool,
    /// Unix datagram socket path
    #[structopt(name = "path", parse(from_os_str), long = "socket-path")]
    socket_path: Option<PathBuf>,
    /// Unix datagram socket file descriptor
    #[structopt(long)]
    fd: Option<i32>,
    /// Incoming migration data
    #[structopt(long = "incoming-fd")]
    incoming_fd: Option<i32>,

    #[structopt(flatten)]
    slirp: libslirp::Opt,
}

fn set_exit_with_parent() {
    #[cfg(any(target_os = "linux", target_os = "android"))]
    unsafe {
        libc::prctl(libc::PR_SET_PDEATHSIG, libc::SIGTERM, 0, 0, 0);
    }
}

const DBUS_TOKEN: Token = Token(10_000_000);

fn from_mioready(ready: mio::Ready) -> c_uint {
    let ready = UnixReady::from(ready);

    0 + if ready.is_readable() {
        dbus::WatchEvent::Readable as c_uint
    } else {
        0
    } + if ready.is_writable() {
        dbus::WatchEvent::Writable as c_uint
    } else {
        0
    } + if ready.is_error() {
        dbus::WatchEvent::Error as c_uint
    } else {
        0
    } + if ready.is_hup() {
        dbus::WatchEvent::Hangup as c_uint
    } else {
        0
    }
}

fn slirp_state_read<'a, R: Read>(
    slirp: &mut libslirp::MioHandler<'a>,
    reader: &mut R,
) -> Result<(), Box<dyn Error>> {
    let mut buf = [0; 4];
    reader.read(&mut buf)?;
    let in_version = i32::from_be_bytes(buf);
    if in_version > libslirp::state_version() {
        panic!(
            "Incompatible migration data version: {} > {}",
            in_version,
            libslirp::state_version()
        );
    }

    slirp.ctxt.state_read(in_version, reader)?;
    slirp.register();
    Ok(())
}

fn dbus_dispatch<'a>(
    slirp: &mut libslirp::MioHandler<'a>,
    event: &Event,
    dbus: &dbus::Connection,
    dbus_fd: RawFd,
    dbus_id: &String,
) -> Result<(), Box<dyn Error>> {
    let dbus_introspect = r#"
    <!DOCTYPE node PUBLIC "-//freedesktop//DTD D-BUS Object Introspection 1.0//EN"
    "http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd">
    <node name="/">
     <node name="org/freedesktop/Slirp1/Helper">
       <interface name="org.freedesktop.Slirp1.Helper">
         <method name="GetInfo">
           <arg name="info" type="s" direction="out"/>
         </method>
       </interface>
     </node>
     <node name="org/qemu/VMState1">
       <interface name="org.qemu.VMState1">
         <property name="Id" type="s" access="read"/>
         <method name="Load">
           <arg name="data" type="ay" direction="in"/>
         </method>
         <method name="Save">
           <arg name="data" type="ay" direction="out"/>
         </method>
       </interface>
     </node>
    </node>
    "#;

    let m = from_mioready(event.readiness());
    for e in dbus.watch_handle(dbus_fd, m) {
        match e {
            dbus::ConnectionItem::MethodCall(m) => {
                if m.interface() == Some("org.freedesktop.Slirp1.Helper".into()) {
                    if m.member() == Some("GetInfo".into()) {
                        dbus.send(m.method_return().append(slirp.ctxt.connection_info()))
                            .unwrap();
                        continue;
                    }
                } else if m.interface() == Some("org.qemu.VMState1".into()) {
                    if m.member() == Some("Load".into()) {
                        let data: Vec<u8> = m.read1()?;
                        slirp_state_read(slirp, &mut data.as_slice())?;
                        dbus.send(m.method_return()).unwrap();
                        continue;
                    }
                    if m.member() == Some("Save".into()) {
                        let mut data = libslirp::state_version().to_be_bytes().to_vec();
                        data.append(&mut slirp.ctxt.state_get()?);
                        dbus.send(m.method_return().append(&*data)).unwrap();
                        continue;
                    }
                } else if m.interface() == Some("org.freedesktop.DBus.Properties".into()) {
                    let id = dbus::arg::Variant(Box::new(dbus_id));

                    if m.member() == Some("Get".into()) {
                        dbus.send(m.method_return().append1(id)).unwrap();
                        continue;
                    } else if m.member() == Some("GetAll".into()) {
                        let mut map = HashMap::new();
                        map.insert("Id", id);
                        dbus.send(m.method_return().append1(map)).unwrap();
                        continue;
                    }
                } else if m.interface() == Some("org.freedesktop.DBus.Introspectable".into())
                    && m.member() == Some("Introspect".into())
                {
                    let mut mr = dbus::Message::new_method_return(&m).unwrap();
                    mr.append_items(&[dbus_introspect.into()]);
                    dbus.send(mr).unwrap();
                    continue;
                }

                dbus.send(
                    dbus::Message::new_error(
                        &m,
                        "org.freedesktop.DBus.Error.UnknownMethod",
                        "Unknown method",
                    )
                    .unwrap(),
                )
                .unwrap();
            }
            _ => {
                continue;
            }
        }
    }

    Ok(())
}

fn print_capabilities() -> Result<(), Box<dyn Error>> {
    io::stdout().write_all(
        r#"{
  "type": "slirp-helper",
  "features": [
    "dbus-address",
    "dbus-p2p",
    "exit-with-parent",
    "migrate",
    "tftp",
    "ipv4",
    "ipv6",
    "restrict"
  ]
}
"#
        .as_bytes(),
    )?;

    Ok(())
}

extern "C" {
    fn dbus_p2p(addr: *const c_char, id: *const c_char, slirp: *mut c_void, mig: c_int);
}

fn main() -> Result<(), Box<dyn Error>> {
    let mut opt = Opt::from_args();
    if opt.debug {
        dbg!(&opt);
    }
    if opt.print_capabilities {
        return print_capabilities();
    }

    let stream = match &opt {
        Opt {
            fd: Some(_),
            socket_path: Some(_),
            ..
        } => panic!("Please specify either a fd or a path"),
        Opt { fd: Some(fd), .. } => unsafe { UnixDatagram::from_raw_fd(*fd) },
        Opt {
            socket_path: Some(path),
            ..
        } => UnixDatagram::bind(path)?,
        _ => panic!("Missing a socket argument"),
    };

    if opt.exit_with_parent {
        set_exit_with_parent();
    }

    let mut dbus_fd = -1;
    let dbus = if let Some(dbus_addr) = opt.dbus_address {
        if opt.dbus_id.is_none() {
            panic!("You must specify an id with DBus");
        }
        let c = dbus::Connection::open_private(&dbus_addr)?;
        c.register_object_path("/org/freedesktop/Slirp1/Helper")?;
        c.register_object_path("/org/qemu/VMState1")?;
        if c.register().is_err() && opt.debug {
            eprintln!("Failed to register to the DBus bus, ignoring");
        } else {
            c.register_name(&format!("org.freedesktop.Slirp1_{}", process::id()), 0)?;
            c.register_name(&"org.qemu.VMState1", 0)?;
        }
        Some(c)
    } else {
        None
    };

    let poll = Poll::new()?;
    if let Some(ref dbus) = dbus {
        let fds = dbus.watch_fds();
        assert_eq!(fds.len(), 1); // very limited support atm
        dbus_fd = fds[0].fd();
        poll.register(
            &EventedFd(&dbus_fd),
            DBUS_TOKEN,
            Ready::readable(),
            PollOpt::level(),
        )?;
    }

    let mut slirp = libslirp::MioHandler::new(&opt.slirp, &poll, stream);

    if let Some(p2p_addr) = opt.dbus_p2p {
        eprintln!("HACK: dbus-p2p server");
        let p2p_addr = CString::new(p2p_addr).unwrap();
        let id = CString::new(opt.dbus_id.as_ref().unwrap().as_bytes()).unwrap();
        let slirp = slirp.ctxt.inner.context;
        unsafe {
            dbus_p2p(
                p2p_addr.as_ptr(),
                id.as_ptr(),
                slirp as *mut c_void,
                opt.dbus_incoming as c_int,
            );
        }
        opt.dbus_incoming = false;
    }

    if opt.dbus_incoming && opt.incoming_fd.is_some() {
        panic!("Invalid multiple incoming paths.")
    }

    let mut events = Events::with_capacity(1024);
    let mut duration = None;

    if let Some(fd) = opt.incoming_fd {
        let mut f = unsafe { File::from_raw_fd(fd) };
        slirp_state_read(&mut slirp, &mut f)?;
    } else if !opt.dbus_incoming {
        slirp.register();
    }

    loop {
        if opt.debug {
            dbg!(duration);
        }

        poll.poll(&mut events, duration)?;
        duration = slirp.dispatch(&events)?;
        if let Some(ref dbus) = dbus {
            for event in &events {
                match event.token() {
                    DBUS_TOKEN => dbus_dispatch(
                        &mut slirp,
                        &event,
                        dbus,
                        dbus_fd,
                        opt.dbus_id.as_ref().unwrap(),
                    )?,
                    _ => {
                        continue;
                    }
                }
            }
        }
    }
}
